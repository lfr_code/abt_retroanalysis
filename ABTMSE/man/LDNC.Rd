% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Performance_metrics.R
\name{LDNC}
\alias{LDNC}
\title{Relative SSB (SSB relative to zero fishing) over all projection years (a performance metrics function of class PM)}
\usage{
LDNC(MSE, pp = 1)
}
\arguments{
\item{MSE}{An object of class MSE}
}
\value{
a matrix with n Management procedures (MSE@nMP) and nsim (MSE@nsim) columns from \code{MSE}
}
\description{
Relative SSB (SSB relative to zero fishing) over all projection years (a performance metrics function of class PM)
}
\examples{
loadABT()
LDNC(MSE_example)
}
