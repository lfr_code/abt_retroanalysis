% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/M3_tools.R
\name{make_summary_report}
\alias{make_summary_report}
\title{Make M3 summary fitting report for all OMs}
\usage{
make_summary_report(dir, OMdirs = NA)
}
\arguments{
\item{dir}{character: an folder containing subfolders that are fitted M3 OMs}

\item{OMdirs}{character vector: specific subfolders representing a subset of OMs you would like to compare in the summary report}
}
\value{
a pdf report in the directory dir
}
\description{
Make M3 summary fitting report for all OMs
}
