# Effects of phase-in periods for ABT CMPs

This bitbucket repository contains the code used for an analysis 
of the effects of phase-in periods for Atlantic Bluefin Tuna (ABT) 
candidate management procedures (CMP). The findings are reported in the
draft SCRS document SCRS/2021/152, by Johnson and Cox of 
Landmark Fisheries Research, an annotated and revised version
of which is included in the writeUp/ folder in this repository.

This work was conducted as deliverable of a contract for
consulting services between Landmark Fisheries Research and Pew 
Charitable Trusts. All contents of this repository except for
the ABT-MSE R package are the intellectual property of Pew
Charitable Trusts.


# General structure of the repository and project

The main working directory contains all R scripts used to
conduct the sensitivity analysis. The following text describes
the functionality/contents of most R scripts in order 
of their application in the analysis, so that dependencies 
are described before work that depends on them. Detailed instructions
for running the BR CMP follow this general outline.

Contributed CMPs are functions, defined in individual R scripts, i.e.

- BR3.R contains the functions for the BR CMP
- constU.R contains the functions for the LW CMP
- Fzero1.R contains the functions for the AH CMP

where BR, LW, and AH correspond to the CMP names used in the 
SCRS paper reporting this analysis.

**initTest.R** loads (and installs if necessary) the
required R packages, as well as installing the ABT-MSE 
package on the first source.

There are three scripts that contain functions libraries used
in the analysis:

- **runCMPs.R** contains a function to run a list of CMPs over
the full grid of operating models and save the MSE objects
to a nominated project folder
- **tools.R** contains functions that are used to help reproducibility
and repeatability of the analysis, and calculate performance metric
tables and generate response surface objects
- **plots.R** contains functions to generate any plots


CMPs were tuned to Br30 = 1.25 for each stock, but the process
is not described in readme. The process for obtaining the
target tuning parameters followed the grid-search tuning
approach taken by the same authors in the previous tuning analysis
paper (SCRS/2021/123; https://bitbucket.org/lfr_code/tuninganalysis/).

The retrospective analysis is completed by sourcing the **runRetros.R**
script. The script takes less than a minute to execute after the
ABTMSE package and all objects are loaded. After running, the 
script outputs three figures to the ./figs/ working directory.

Phase-in experiments have separate scripts for each CMP, and
deterministic and stochastic operating models. Deterministic
OMs are run by sourcing **runPhase_X.R** scripts (2-3 hours), 
and **runPhase_X_stoch.R** for stochastic operating models (approx.
48 hours). All runPhase scripts complete by saving MSE objects
to a project folder within the ./MSEs/ folder, and automatically 
computing and saving performance metrics in the same project folder. 




## Notes: 

1. These scripts are not meant to be run line-by-line in interactive 
mode, as the analyses take several hours. Instead, scripts are programs 
that will output results to the MSEs/ folder when they terminate 
succesfully. 
2. Scripts do not clear the environment via ``rm(list = ls())``
or change the working directory. To avoid errors, please ensure that 
your R environment is clear and the working directory is correct before 
sourcing any scripts in the analysis.
3. Analyses are run by sourcing one of the above mentioned scripts in
a fresh R session, and leaving the computer to run for the required amount
of time. Approximate run times are given at the top of some scripts.
4. ABT-MSE package requires 4 - 7 GB of memory for a run of 25 CMPs
on a single deterministic OM, so plan accordingly
5. LW CMP is sometimes referred to as conU or constU in the code
6. AH CMP is sometimes referred to as F01 or Fzero1 in the code



# Detailed instructions for how to re-run the analysis

Below is an enumerated list for how to run the analysis and generate
all resulting plots and tables of performance metrics. For all steps,
we suggest the user opens a fresh R instance and closes instances
from previous steps, as the ABTMSE simulation package is very memory
intensive (several gigabytes per simulation run).

1. Source *initTest.R* to ensure all packages install successfully
2. Source *runRetros.R* to perform retrospective analysis and generate 
results, close R instance
3. Open a new R instance, and source *runPhase_BR.R* to run the 
deterministic simulations for the BR CMP (about 1-2 hours). Close
the R instance when complete.
4. Open a new R instance, and source *runPhase_LW.R* to run the 
deterministic simulations for the LW CMP (about 1-2 hours). Close
the R instance when complete.
5. Open a new R instance, and source *runPhase_AH.R* to run the 
deterministic simulations for the AH CMP (about 1-2 hours). Close
the R instance when complete.
6. Open a new R instance, and source *runPhase_BR_stoch.R* to run the 
stochastic simulations for the BR CMP (about 24-48 hours). Close
the R instance when complete.
7. Open a new R instance, and source *runPhase_LW_stoch.R* to run the 
stochastic simulations for the LW CMP (about 24-48 hours). Close
the R instance when complete.
8. Open a new R instance, and source *runPhase_AH_stoch.R* to run the 
stochastic simulations for the AH CMP (about 24-48 hours). Close
the R instance when complete.

There are efficiency gains that can be made via parallel processing 
to speed up simulations. For example, if you have the available memory, steps
3 - 5 may be run at the same time, as they take up less memory (about
4gb - 7gb per step). Unfortunately, the stochastic OMs in steps 6 - 8 require 
much more memory, so unless you have access to high peformance computers
these will need to be run individually.

One option to speed up steps 6 - 8 is to split the OMs across multiple
machines and calculate performance metrics manually once they are 
recombined. Feel free to contact the authors for guidance on this if 
you require.


